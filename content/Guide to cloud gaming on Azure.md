+++
title = "Guide to cloud gaming on Azure"
description = "A guide to setup your own private cloud gaming server on Azure."
date = 2022-02-26T03:33:00+01:00

[taxonomies]
tags = ["Cloud gaming", "Azure", "Remote desktop", "Remote gaming", "Parsec", "windows 10"]
+++

This article is heavily inspired from [Medium - A killer guide for Cloud Gaming on Azure (March 2020)](https://medium.com/azure-cloud/a-killer-guide-for-cloud-gaming-on-azure-march-2020-1aa56d13fba3). I've decided to rewrite my own article to clarify some details and to add some information.

In this article we'll see how to setup a gaming cloud grade Windows 10 Pro on Azure in order to remotely play games such as Fallout 4.

## Requirementss

- A local **Windows 10** desktop
- An [**Azure**](https://portal.azure.com) account
- A [**Parsec**](https://parsec.app/) account

## Create Azure VM

I'll assume you already have an Azure account and go straight to the VM creation

Go to [Create a virtual machine](https://portal.azure.com/#create/Microsoft.VirtualMachine)

### Required fields:

- Give it a **name**, example: GamingCloud
- Choose a **Windows 10 Pro Gen 1 image**, and not a windows server image. Check your [VM size](https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-gpu) for compatibility **Gen 1** or **2**.
- Choose a [**NV-series**](https://docs.microsoft.com/en-us/azure/virtual-machines/nv-series) or [**NVv3-series**](https://docs.microsoft.com/en-us/azure/virtual-machines/nvv3-series) VM size for its Nvidia Tesla GPU, example: **Standard_NV6**.
- Fill the **Administrator account's username and password fields**. This will be the account used by the windows 10 pro image itself, to not confuse with your Azure account
- Inbound port rules
  - Public inbound ports: **Allow selected ports**
  - Select inbound ports: **RDP (3389)**
- Disk options: **Standard HDD**

### Optional fields:

- **Create and attach a new disk**, **Standard HDD**, **Delete with VM**, with the desired size to install your games. The temporary disk may not be big enough for them. Here the Standard_NV6's disk size is 380GB. 
- Enable **Auto-shutdown**, this instance comes at a high price, $1.595/hour in West Europe. An auto-shutdown early in the morning can save you a lot of money if you forget to shut it down even once.

### Finaly

- **Create!**

The rest of the fields are left to their default value. But feel free to tweak some of them for your own needs.

## Connect to your remote Windows 10 Pro

Once your VM is deployed, we'll connect to it with [Microsoft Remote Desktop](https://www.microsoft.com/en-us/p/microsoft-remote-desktop/9wzdncrfj3ps). Other solutions exists, but this is my personal favorite, easy and native to Windows 10.

- Download [Microsoft Remote Desktop](https://www.microsoft.com/en-us/p/microsoft-remote-desktop/9wzdncrfj3ps) from the microsoft store
- Use your **VM's public IP address** as the **PC name** of the connection
- Add the **Administrator account** as the **User account**
- **Connect!**



## Setup Windows 10's GPU and Parsec

In order to use the **GPU** we have to manually setup its driver on our **remote Windows 10 Pro**. We will then use [Parsec](https://parsec.app/) as our remote cloud gaming tool.

- **Check for updates** and install latest updates.

- Download latest **graphic drivers** [NVIDIA TESLA M60](https://www.nvidia.com/Download/driverResults.aspx/158195/en-us)

- [Download Parsec](https://parsec.app/downloads), install it and log in (You need a Parsec account)

- Check in **Startup Apps** that Parsec is on

- **Restart** remote Windows 10 pro

- Get your GPU bus ID by running in PowerShell

  ```powershell
  # Default file path
  cd 'C:\Program Files\NVIDIA Corporation\NVSMI'
  .\nvidia-smi
  ```

- Run in PowerShell, to set the TESLA monitor by default

  ```powershell
  # Default file path
  cd 'C:\Program Files\NVIDIA Corporation\NVSMI'
  .\nvidia-smi -g {BUS_ID} -dm 0 # example: .\nvidia-smi -g 00000001:00:00.0 -dm 0
  ```

- Add an audio device to enable audio. Install [VBCABLE](https://vb-audio.com/Cable/)

- **Restart** remote Windows 10 pro



## Starting a Gaming session

Now that everything is installed we'll see the steps to start a gaming session on your new gaming cloud system.

- Start your Azure Virtual Machine
- Connect to your Windows 10 with **Microsoft Remote Desktop** to initiate a RDP session and start windows 10.
- Close your **Microsoft Remote Desktop**, your RDP session.
- Connect to it with [Parsec](https://parsec.app/)
- Play, install, do whatever you wish through [Parsec](https://parsec.app/)!
- Don't forget to shutdown when you're finished to save money when you're done.



We're all done, your gaming cloud system is ready to go. All there is left is for you to install your favorite games and play them! Thank you for reading this post, hoping it has been useful.



## Resources

Here's a list of articles that helped me write my own:

- [Medium - A killer guide for Cloud Gaming on Azure (March 2020)](https://medium.com/azure-cloud/a-killer-guide-for-cloud-gaming-on-azure-march-2020-1aa56d13fba3)
- [Larry Land - Cloudy Gamer: Playing Overwatch on Azure's new monster GPU instances](https://lg.io/2016/10/12/cloudy-gamer-playing-overwatch-on-azures-new-monster-gpu-instances.html)
- [Github - Parsec Cloud Preparation Tool](https://github.com/parsec-cloud/Parsec-Cloud-Preparation-Tool)
- [Parsec - Error Codes - 22006 and 15000 (Unable To Initialize Encoder On Your Server)](https://support.parsec.app/hc/en-us/articles/115002624051-Error-Codes-22006-and-15000-Unable-To-Initialize-Encoder-On-Your-Server-)
- [Parsec - Error Codes - 14003 (Failed to Capture the Host Display)](https://support.parsec.app/hc/en-us/articles/360002165172-Error-Codes-14003-Failed-To-Capture-The-Host-Display-)
- [Microsoft - GPU optimized virtual machine sizes](https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-gpu)
- [Microsoft - NV-series](https://docs.microsoft.com/en-us/azure/virtual-machines/nv-series)
- [reddit - azure nv6 using libx264 instead of nvenc](https://www.reddit.com/r/cloudygamer/comments/5hmzv5/azure_nv6_using_libx264_instead_of_nvenc/db1xje8/)
