+++
title = "About"
template = "about/section.html"
+++

Hello everybody,

My name is <b>Boris Laporte</b> , I am a French back-end developer based in Paris and a beginner DevOps when the need arises.


I love traveling around the world, discovering new things, meeting new people and share it all throught photography.
